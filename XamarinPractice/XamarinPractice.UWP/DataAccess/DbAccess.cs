﻿using SQLite;
using System.IO;
using Windows.Storage;
using Xamarin.Forms;
using XamarinPractice.DataAccess;
using XamarinPractice.UWP.DataAccess;

[assembly: Dependency(typeof(DbAccess))]
namespace XamarinPractice.UWP.DataAccess
{
	public class DbAccess : IDbAccess
	{
		public SQLiteAsyncConnection GetConnection()
		{
			var documentsPath = ApplicationData.Current.LocalFolder.Path;
			var path = Path.Combine(documentsPath, "MySQLite.db3");
			return new SQLiteAsyncConnection(path);
		}

		public string GetPath()
		{
			var documentsPath = ApplicationData.Current.LocalFolder.Path;
			return Path.Combine(documentsPath, "MySQLite.db3");
		}
	}
}
