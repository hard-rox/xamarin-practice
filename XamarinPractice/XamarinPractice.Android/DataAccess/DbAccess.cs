﻿using SQLite;
using System;
using System.IO;
using Xamarin.Forms;
using XamarinPractice.DataAccess;
using XamarinPractice.Droid.DataAccess;

[assembly: Dependency(typeof(DbAccess))]
namespace XamarinPractice.Droid.DataAccess
{
	public class DbAccess : IDbAccess
	{
		public SQLiteAsyncConnection GetConnection()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var path = Path.Combine(documentsPath, "MySQLite.db3");

			return new SQLiteAsyncConnection(path);
		}

		public string GetPath()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			return Path.Combine(documentsPath, "MySQLite.db3");
		}
	}
}