﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using SQLite;
using UIKit;
using Xamarin.Forms;
using XamarinPractice.DataAccess;
using XamarinPractice.iOS.DataAccess;

[assembly: Dependency(typeof(DbAccess))]
namespace XamarinPractice.iOS.DataAccess
{
	class DbAccess : IDbAccess
	{
		public SQLiteAsyncConnection GetConnection()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			var path = Path.Combine(documentsPath, "MySQLite.db3");

			return new SQLiteAsyncConnection(path);
		}

		public string GetPath()
		{
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			return Path.Combine(documentsPath, "MySQLite.db3");
		}
	}
}