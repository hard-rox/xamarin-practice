﻿using System.Collections.ObjectModel;
using XamarinPractice.MVVMTry.Models;

namespace XamarinPractice.MVVMTry.ViewModels
{
    public class PlaylistPageViewModel
    {
		public ObservableCollection<Playlist> Playlists { get; private set; } = new ObservableCollection<Playlist>();

		public void AddPlaylist()
		{
			Playlists.Add(new Playlist() { Name = "Playlist " + (Playlists.Count + 1) });
		}
	}
}
