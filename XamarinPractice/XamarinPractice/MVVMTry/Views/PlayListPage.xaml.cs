﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.MVVMTry.ViewModels;

namespace XamarinPractice.MVVMTry.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PlayListPage : ContentPage
	{
		public PlayListPage ()
		{
			BindingContext = new PlaylistPageViewModel();
			InitializeComponent ();
		}

		private void Button_Clicked(object sender, EventArgs e)
		{
			((PlaylistPageViewModel)BindingContext).AddPlaylist();
		}
	}
}