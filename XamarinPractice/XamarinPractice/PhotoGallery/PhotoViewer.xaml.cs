﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.PhotoGallery
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PhotoViewer : ContentPage
	{
		private int imageId = 1;
		public PhotoViewer ()
		{
			InitializeComponent ();
			image.Source = "https://picsum.photos/1920/1080/?image=" + imageId;
		}

		private void PrevBtn_Clicked(object sender, EventArgs e)
		{
			imageId = imageId == 1 ? 1 : imageId--;
			image.Source = "https://picsum.photos/1920/1080/?image=" + imageId;
			DisplayAlert("Alert", "You have been alerted", "OK");
		}

		private void NxtBtn_Clicked(object sender, EventArgs e)
		{
			imageId++;
			image.Source = "https://picsum.photos/1920/1080/?image=" + imageId;
			DisplayAlert("Alert", "You have been alerted", "OK");
		}
	}
}