﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.Models;

namespace XamarinPractice.ListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class GroupList : ContentPage
	{
		public GroupList()
		{
			InitializeComponent();

			var peopleGroups = new List<PeopleGroup>()
			{
				new PeopleGroup("Aaaa", "A")
				{
					new People(){Id = 1, Name = "Mr. A", Status = "", ImageUrl = "https://picsum.photos/100/100/?image=1"},
					new People(){Id = 2, Name = "Mr. B", Status = "Go to hell...", ImageUrl = "https://picsum.photos/100/100/?image=2"},
					new People(){Id = 3, Name = "Mr. C", Status = "Bohemian...", ImageUrl = "https://picsum.photos/100/100/?image=3"},
					new People(){Id = 4, Name = "Mr. D", Status = "Rocking...", ImageUrl = "https://picsum.photos/100/100/?image=4"}
				},
				new PeopleGroup("Bbbb", "B")
				{
					new People(){Id = 1, Name = "Mr. B", Status = "", ImageUrl = "https://picsum.photos/100/100/?image=1"},
					new People(){Id = 2, Name = "Mr. B", Status = "Go to hell...", ImageUrl = "https://picsum.photos/100/100/?image=2"},
					new People(){Id = 3, Name = "Mr. B", Status = "Bohemian...", ImageUrl = "https://picsum.photos/100/100/?image=3"},
					new People(){Id = 4, Name = "Mr. B", Status = "Rocking...", ImageUrl = "https://picsum.photos/100/100/?image=4"}
				}
			};

			listView.ItemsSource = peopleGroups;
		}
	}
}