﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.ListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class BasicList : ContentPage
	{
		public BasicList ()
		{
			InitializeComponent ();

			var nameList = new List<string>()
			{
				"Roxy",
				"Mr. A",
				"Mr. B",
				"Miss. Sexy"
			};

			listView.ItemsSource = nameList;
		}
	}
}