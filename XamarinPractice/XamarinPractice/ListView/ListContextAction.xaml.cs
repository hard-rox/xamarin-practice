﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.Models;

namespace XamarinPractice.ListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ListContextAction : ContentPage
	{
		private ObservableCollection<People> peoples = new ObservableCollection<People>();
		public ListContextAction ()
		{
			InitializeComponent ();

			peoples = new ObservableCollection<People>()
			{
				new People(){Id = 1, Name = "Mr. A", Status = "Sleeping...", ImageUrl = "https://picsum.photos/100/100/?image=1"},
				new People(){Id = 2, Name = "Mr. B", Status = "Go to hell...", ImageUrl = "https://picsum.photos/100/100/?image=2"},
				new People(){Id = 3, Name = "Mr. C", Status = "Bohemian...", ImageUrl = "https://picsum.photos/100/100/?image=3"},
				new People(){Id = 4, Name = "Mr. D", Status = "Rocking...", ImageUrl = "https://picsum.photos/100/100/?image=4"}
			};

			listView.ItemsSource = peoples;
		}

		private void Call_Clicked(object sender, EventArgs e)
		{
			var people = (People)(((MenuItem)sender).CommandParameter);
			DisplayAlert("Calling", people.Name, "OK");
		}

		private void Delete_Clicked(object sender, EventArgs e)
		{
			var people = (People)(((MenuItem)sender).CommandParameter);
			peoples.Remove(people);
			DisplayAlert("Deleted", people.Name, "OK");
		}
	}
}