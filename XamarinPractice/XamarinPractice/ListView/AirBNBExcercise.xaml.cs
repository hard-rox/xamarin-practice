﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.Models;

namespace XamarinPractice.ListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class AirBNBExcercise : ContentPage
	{
		private ObservableCollection<SearchGroup> searchList;

		private void GetSearchList()
		{
			//Group ObservableCollection is not reflects deletes on listview...
			searchList = new ObservableCollection<SearchGroup>()
			{
				new SearchGroup("Recent Search", "Recent")
				{
					new Search(){Location = "Rupnagar R/A, Mirpur-2, Dhaka 1216", CheckIn = DateTime.Now.AddDays(7), CheckOut = DateTime.Now},
					new Search(){Location = "Nilkhet Rd, Dhaka 1000", CheckIn = DateTime.Now.AddDays(7), CheckOut = DateTime.Now}
				},

				new SearchGroup("All Search", "All")
				{
					new Search(){Location = "Rupnagar R/A, Mirpur-2, Dhaka 1216", CheckIn = DateTime.Now.AddDays(7), CheckOut = DateTime.Now},
					new Search(){Location = "Nilkhet Rd, Dhaka 1000", CheckIn = DateTime.Now.AddDays(7), CheckOut = DateTime.Now},
					new Search(){Location = "Savar Union 1342", CheckIn = DateTime.Now.AddDays(7), CheckOut = DateTime.Now},
					new Search(){Location = "Plot, 15, Block B Kuril - NSU Rd, Dhaka 1229", CheckIn = DateTime.Now.AddDays(7), CheckOut = DateTime.Now}
				},
			};
		}
		public AirBNBExcercise ()
		{
			InitializeComponent ();
			GetSearchList();
			SearchList.ItemsSource = searchList;
		}

		private void SearchBar_TextChanged(object sender, TextChangedEventArgs e)
		{
			//searchList = searchList.ToList().ForEach(
			//	g => g.Where(s => s.Location.Contains(e.NewTextValue)));
			var searchResult = new ObservableCollection<SearchGroup>();
			foreach(var group in searchList)
			{
				var g = new SearchGroup(group.GroupName, group.Index);
				g.AddRange(group.Where(grp => grp.Location.ToUpper().Contains(e.NewTextValue.ToUpper())));
				searchResult.Add(g);
			}
			SearchList.ItemsSource = searchResult;
		}

		private void SearchList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{

		}

		private void Delete_Clicked(object sender, EventArgs e)
		{
			var search = (Search)(((MenuItem)sender).CommandParameter);
			//DisplayAlert("delete", search.Location, "Ok");
			foreach (var group in searchList)
			{
				if(group.IndexOf(search) >= 0)
					group.Remove(search);
			}
			SearchList.ItemsSource = searchList;
		}
	}
}