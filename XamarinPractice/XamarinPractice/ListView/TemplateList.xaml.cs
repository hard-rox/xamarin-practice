﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.Models;

namespace XamarinPractice.ListView
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class TemplateList : ContentPage
	{
		public TemplateList ()
		{
			InitializeComponent ();

			var contacts = new List<People>()
			{
				new People(){Id = 1, Name = "Mr. A", Status = "Sleeping...", ImageUrl = "https://picsum.photos/100/100/?image=1"},
				new People(){Id = 2, Name = "Mr. B", Status = "Go to hell...", ImageUrl = "https://picsum.photos/100/100/?image=2"},
				new People(){Id = 3, Name = "Mr. C", Status = "Bohemian...", ImageUrl = "https://picsum.photos/100/100/?image=3"},
				new People(){Id = 4, Name = "Mr. D", Status = "Rocking...", ImageUrl = "https://picsum.photos/100/100/?image=4"}
			};

			listView.ItemsSource = contacts;
		}
	}
}