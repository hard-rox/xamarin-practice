﻿using SQLite;
using System.ComponentModel;

namespace XamarinPractice.DataAccess
{
    public class Name : INotifyPropertyChanged
    {
		public event PropertyChangedEventHandler PropertyChanged;

		[PrimaryKey, AutoIncrement]
		public int Id { get; set; }

		private string _text;
		public string Text
		{
			get { return _text; }
			set
			{
				if (_text == value) return;
				_text = value;
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Text)));
			}
		}
		public int Count { get; set; }
	}
}
