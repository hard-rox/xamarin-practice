﻿using SQLite;

namespace XamarinPractice.DataAccess
{
    public interface IDbAccess
    {
		SQLiteAsyncConnection GetConnection();
		string GetPath();
    }
}
