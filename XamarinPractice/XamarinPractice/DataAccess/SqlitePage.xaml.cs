﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.DataAccess
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SqlitePage : ContentPage
	{
		private SQLiteAsyncConnection _connection;
		private string _dbPath;
		private ObservableCollection<Name> _texts;
		public SqlitePage ()
		{
			InitializeComponent ();

			_connection = DependencyService.Get<IDbAccess>().GetConnection();
			_dbPath = DependencyService.Get<IDbAccess>().GetPath();
		}

		protected override void OnAppearing()
		{
			_connection.CreateTableAsync<Name>();
			var names = _connection.Table<Name>().ToListAsync().Result;

			_texts = new ObservableCollection<Name>(names);
			NameList.ItemsSource = _texts;
			PathText.Text = _dbPath;
			base.OnAppearing();
		}

		private void AddButton_Clicked(object sender, EventArgs e)
		{
			var obj = new Name() { Text = "hello" + DateTime.Now.Ticks };
			_connection.InsertAsync(obj);
			_texts.Add(obj);
		}

		private void UpdateButton_Clicked(object sender, EventArgs e)
		{
			var name = _texts[0];
			name.Text = name.Text += "Updated..";
			_connection.UpdateAsync(name);
			_texts[0] = name;
		}

		private void DeleteButton_Clicked(object sender, EventArgs e)
		{
			var name = _texts[0];
			_connection.DeleteAsync(name);
			_texts.Remove(name);
		}
	}
}