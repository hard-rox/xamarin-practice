﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Windows.Storage;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.FileIO
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class FileReadWrite : ContentPage
	{
		private string _filePath;
		private List<string> GetFileData()
		{
			var res = File.Exists(_filePath) ? File.ReadAllLines(_filePath).ToList() : new List<string>();
			return res;
		}
		public FileReadWrite()
		{
			InitializeComponent();

			_filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), "text.txt");
			//Android works good... UWP not...
			//DisplayAlert("Path", _filePath, "Ok");

			TextList.ItemsSource = GetFileData();
		}

		private void AddText_Clicked(object sender, EventArgs e)
		{
			DisplayAlert("Path", _filePath, "Ok");
			try
			{
				if (!File.Exists(_filePath)) File.Create(_filePath);
				using (var writer = new StreamWriter(_filePath, true))
				{
					writer.WriteLine(NewText.Text);
				}
				DisplayAlert("Done", "Text writen...", "Ok");
			}
			catch (Exception ex)
			{
				DisplayAlert("Exception", ex.ToString(), "Ok");
			}
			NewText.Text = "";
			TextList.ItemsSource = GetFileData();
		}
	}
}