﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.UserInputs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Pickers : ContentPage
	{
		public Pickers ()
		{
			InitializeComponent ();
		}

		private void DatePickerTest_DateSelected(object sender, DateChangedEventArgs e)
		{

		}

		private void TimePickerTest_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
		{
			//fires 3 times...
			DisplayAlert("Time", TimePickerTest.Time.ToString(), "Ok");
		}
	}
}