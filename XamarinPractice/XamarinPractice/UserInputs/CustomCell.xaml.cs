﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.UserInputs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomCell : ViewCell
	{
		public static readonly BindableProperty LabelTextProperty = BindableProperty.Create("LabelText", typeof(string), typeof(CustomCell));
		public string LabelText
		{
			get { return (string)GetValue(LabelTextProperty); }
			set { SetValue(LabelTextProperty, value); }
		}
		public CustomCell ()
		{
			InitializeComponent ();

			BindingContext = this;
		}
	}
}