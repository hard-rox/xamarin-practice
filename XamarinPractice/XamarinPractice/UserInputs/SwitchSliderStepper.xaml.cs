﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.UserInputs
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SwitchSliderStepper : ContentPage
	{
		public SwitchSliderStepper ()
		{
			InitializeComponent ();
		}
	}
}