﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.UserInputs.ContactBook
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactList : ContentPage
	{
		public ContactList ()
		{
			InitializeComponent ();

			ContactListView.ItemsSource = ContactService.GetContacts();
		}

		private void ContactListView_Refreshing(object sender, EventArgs e)
		{
			ContactListView.ItemsSource = ContactService.GetContacts();
			ContactListView.EndRefresh();
		}

		private void ContactListView_ItemTapped(object sender, ItemTappedEventArgs e)
		{
			var contact = (Contact)e.Item;
			Navigation.PushAsync(new ContactDetails(contact));
		}

		private void Button_Clicked(object sender, EventArgs e)
		{
			Navigation.PushAsync(new ContactDetails(new Contact()));
		}
	}
}