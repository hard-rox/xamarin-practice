﻿using System.Collections.Generic;

namespace XamarinPractice.UserInputs.ContactBook
{
	public static class ContactService
	{
		public static List<Contact> contacts = new List<Contact>()
			{
				new Contact() { Id = 1, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
				new Contact() { Id = 2, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
				new Contact() { Id = 3, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
				new Contact() { Id = 4, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
				new Contact() { Id = 5, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
				new Contact() { Id = 6, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
				new Contact() { Id = 7, FirstName = "Hello", LastName = "World", Phone = "123456789", Email = "a@b.c", IsBlocked = false },
			};
		public static List<Contact> GetContacts()
		{
			return contacts;
		}
	}
}
