﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.UserInputs.ContactBook
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactDetails : ContentPage
	{
		private Contact contact;
		public ContactDetails (Contact contact)
		{
			InitializeComponent ();

			this.contact = contact;
			BindingContext = this.contact;
		}

		private void Button_Clicked(object sender, EventArgs e)
		{
			var updatedContact = ContactService.contacts.FirstOrDefault(c => c.Id == this.contact.Id);
			updatedContact = this.contact;
		}
	}
}