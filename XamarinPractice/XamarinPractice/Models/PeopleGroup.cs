﻿using System.Collections.Generic;

namespace XamarinPractice.Models
{
    public class PeopleGroup : List<People>
    {
		public string GroupTitle { get; set; }
		public string Index { get; set; }

		public PeopleGroup(string groupTitle, string index)
		{
			this.GroupTitle = groupTitle;
			this.Index = index;
		}
	}
}
