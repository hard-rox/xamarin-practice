﻿namespace XamarinPractice.Models
{
    public class People
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string Status { get; set; }
		public string ImageUrl { get; set; }
	}
}
