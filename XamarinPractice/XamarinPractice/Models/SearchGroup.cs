﻿using System.Collections.Generic;

namespace XamarinPractice.Models
{
    public class SearchGroup : List<Search>
    {
		public string GroupName { get; set; }
		public string Index { get; set; }
		public SearchGroup(string groupName, string index)
		{
			this.GroupName = groupName;
			this.Index = index;
		}
    }
}
