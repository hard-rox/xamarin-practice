﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.Navigation.HierarcicalNavgation
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class IntroPage : ContentPage
	{
		public IntroPage ()
		{
			InitializeComponent ();
		}

		async private void BackButton_Clicked(object sender, EventArgs e)
		{
			await Navigation.PopAsync();
		}

		protected override bool OnBackButtonPressed()
		{
			//True for back button disabling...
			//return true;
			return base.OnBackButtonPressed();
		}
	}
}