﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.Navigation.ToolBarItems
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ToolbarLanding : ContentPage
	{
		public ToolbarLanding ()
		{
			InitializeComponent ();
		}
	}
}