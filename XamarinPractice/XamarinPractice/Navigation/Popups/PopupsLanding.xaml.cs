﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.Navigation.Popups
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PopupsLanding : ContentPage
	{
		public PopupsLanding ()
		{
			InitializeComponent ();
		}

		async private void OpenConfirmation_Clicked(object sender, EventArgs e)
		{
			var response = await DisplayAlert("Confirmation", "Are you sure?", "Yes", "No");
			ConfirmationText.Text = response? "true" : "false";
		}

		async private void ActionButton_Clicked(object sender, EventArgs e)
		{
			var res = await DisplayActionSheet("Actions", "Cancel", "Delete", "Button 1", "Button 2", "Button 3");
			ActionText.Text = res;
		}
	}
}