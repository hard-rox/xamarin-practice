﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.Navigation.ModalPage
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ModalPage : ContentPage
	{
		public ModalPage ()
		{
			InitializeComponent ();
		}

		private void CloseButton_Clicked(object sender, EventArgs e)
		{
			Navigation.PopModalAsync();
		}

		protected override bool OnBackButtonPressed()
		{
			return true;
		}
	}
}