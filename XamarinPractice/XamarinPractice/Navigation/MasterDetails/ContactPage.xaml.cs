﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.Models;

namespace XamarinPractice.Navigation.MasterDetails
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactPage : ContentPage
	{
		public ContactPage ()
		{
			InitializeComponent ();

			var contacts = new List<People>()
			{
				new People(){Id = 1, Name = "Mr. A", Status = "Sleeping...", ImageUrl = "https://picsum.photos/100/100/?image=1"},
				new People(){Id = 2, Name = "Mr. B", Status = "Go to hell...", ImageUrl = "https://picsum.photos/100/100/?image=2"},
				new People(){Id = 3, Name = "Mr. C", Status = "Bohemian...", ImageUrl = "https://picsum.photos/100/100/?image=3"},
				new People(){Id = 4, Name = "Mr. D", Status = "Rocking...", ImageUrl = "https://picsum.photos/100/100/?image=4"}
			};

			contactList.ItemsSource = contacts;
		}

		async private void contactList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
		{
			if (e.SelectedItem == null) return;
			await Navigation.PushAsync(new ContactDetailPage(e.SelectedItem as People));
			contactList.SelectedItem = null;
		}
	}
}