﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace XamarinPractice.Navigation.InstagramExercise
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class InstagramLandingPage : TabbedPage
	{
		public InstagramLandingPage ()
		{
			InitializeComponent ();
		}
	}
}