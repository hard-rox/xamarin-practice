﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinPractice.Models;

namespace XamarinPractice.Navigation.InstagramExercise
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ActivityDetails : ContentPage
	{
		public ActivityDetails (People contact)
		{
			BindingContext = contact;
			InitializeComponent ();
		}
	}
}