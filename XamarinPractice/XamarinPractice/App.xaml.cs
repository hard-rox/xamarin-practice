using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace XamarinPractice
{
	public partial class App : Application
	{
		public App ()
		{
			InitializeComponent();

			//MainPage = new PhotoGallery.PhotoViewer();

			//ListView Practice...
			//MainPage = new ListView.BasicList();
			//MainPage = new ListView.TemplateList();
			//MainPage = new ListView.CustomTemplate();
			//MainPage = new ListView.GroupList(); //Not good on Android.
			//MainPage = new ListView.ListSelection();
			//MainPage = new ListView.ListContextAction();
			//MainPage = new ListView.ListPullRefresh();
			//MainPage = new ListView.SearchBar();
			//MainPage = new ListView.AirBNBExcercise();

			//Navigation Practice
			//MainPage = new NavigationPage(new Navigation.HierarcicalNavgation.WelcomePage())
			//{
			//	BarBackgroundColor = Color.Gray,
			//	BarTextColor = Color.White
			//};
			//MainPage = new NavigationPage(new Navigation.ModalPage.HomePage());
			//MainPage = new NavigationPage(new Navigation.MasterDetails.ContactPage());
			//MainPage = new Navigation.ContactsMasterDetail.ContactMasterPage();
			//MainPage = new Navigation.TabPageTry.LandingPage();
			//MainPage = new Navigation.CarouselPageTry.HomeCarousel();
			//MainPage = new Navigation.Popups.PopupsLanding();
			//MainPage = new NavigationPage(new Navigation.ToolBarItems.ToolbarLanding());
			//MainPage = new NavigationPage(new Navigation.InstagramExercise.InstagramLandingPage())
			//{
			//	//nothing happened...
			//	Title = "My Contacts"
			//};

			//User Input Practice...
			//MainPage = new UserInputs.SwitchSliderStepper();
			//MainPage = new UserInputs.Pickers();
			//MainPage = new UserInputs.TableVieTry();
			//MainPage = new NavigationPage(new UserInputs.ContactBook.ContactList());

			//Data Access Try...
			//MainPage = new FileIO.FileReadWrite();
			//MainPage = new DataAccess.SqlitePage();

			//MVVM Practice
			//MainPage = new NavigationPage(new MVVMTry.Views.PlayListPage());

			//UI Practice
			MainPage = new UI_Practice.CaruselControls();
		}

		protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
